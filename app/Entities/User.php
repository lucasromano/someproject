<?php

namespace SomeProject\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Prettus\Repository\Contracts\Transformable;

class User extends Authenticatable implements Transformable
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return array
     */
    public function transform()
    {
        return [
            'id' => $this->id,
            'created_at' => empty($this->created_at) ? null : $this->created_at->format('d/m/Y H:i:s'),
            'name' => $this->name,
            'email' => $this->email,
            'admin' => $this->admin ? true : false,
            'active' => !$this->trashed()
        ];
    }
}