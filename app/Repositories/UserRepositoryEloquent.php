<?php

namespace SomeProject\Repositories;

use Prettus\Repository\Presenter\ModelFractalPresenter;
use SomeProject\Entities\User;

class UserRepositoryEloquent extends SoftDeletesRepository implements UserRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    public function presenter()
    {
        return ModelFractalPresenter::class;
    }
}
