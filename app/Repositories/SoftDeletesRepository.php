<?php

namespace SomeProject\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Events\RepositoryEntityUpdated;
use Prettus\Repository\Helpers\CacheKeys;
use Prettus\Validator\Contracts\ValidatorInterface;

//Extend to support soft deletes
abstract class SoftDeletesRepository extends BaseRepository
{
    public static function clearCache($repository)
    {
        $cacheKeys = CacheKeys::getKeys(get_class($repository));

        $cache = app(config('repository.cache.repository', 'cache'));
        if (is_array($cacheKeys)) {
            foreach ($cacheKeys as $key) {
                $cache->forget($key);
            }
        }
    }

    public function restore($id)
    {
        $entity = $this->model->where('id', $id)->withTrashed()->first();
        $entity->restore();

        if (config("repository.cache.clean.on.restored", true)) {
            SoftDeletesRepository::clearCache($this);
        }
    }

    public function update(array $attributes, $id)
    {
        $this->applyScope();

        if (!is_null($this->validator)) {
            // we should pass data that has been casts by the model
            // to make sure data type are same because validator may need to use
            // this data to compare with data that fetch from database.
            $attributes = $this->model->newInstance()->forceFill($attributes)->toArray();

            $this->validator->with($attributes)->setId($id)->passesOrFail(ValidatorInterface::RULE_UPDATE);
        }

        $temporarySkipPresenter = $this->skipPresenter;

        $this->skipPresenter(true);

        $model = $this->model->withTrashed()->findOrFail($id);
        $model->fill($attributes);
        $model->save();

        $this->skipPresenter($temporarySkipPresenter);
        $this->resetModel();

        event(new RepositoryEntityUpdated($this, $model));

        return $this->parserResult($model);
    }
}