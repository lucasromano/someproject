<?php

namespace SomeProject\Repositories;

use Prettus\Repository\Events\RepositoryEventBase;

class RepositoryEntityRestored extends RepositoryEventBase
{
    /**
     * @var string
     */
    protected $action = "restored";
}