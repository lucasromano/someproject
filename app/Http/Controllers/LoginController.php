<?php

namespace SomeProject\Http\Controllers;

use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Prettus\Validator\Exceptions\ValidatorException;
use SomeProject\Validators\LoginValidator;

class LoginController extends Controller
{
    protected $maxAttempts = 5;
    protected $decayMinutes = 30;

    /**
     * @var LoginValidator
     */
    private $validator;

    use ThrottlesLogins;

    public function __construct(LoginValidator $validator)
    {
        $this->validator = $validator;
    }

    public function index()
    {
        return view('login');
    }

    public function login(Request $request)
    {
        $credentials = [
            'email' => $request->get('email'),
            'password' => $request->get('password'),
        ];

        try {
            $this->validator->with($credentials)->passesOrFail();
        } catch (ValidatorException $e) {
            return response()->json(['success' => false,
                'msgs' => $e->getMessageBag()->all(":message")], 400);
        }

        if ($lockedOut = $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            $minutes = ceil($this->limiter()->availableIn($this->throttleKey($request))/60.0);

            return response()->json(['success' => false, 'msgs' =>
                'Please try again in ' . $minutes . ($minutes == 1 ? ' minute.' : ' minutes.')], 400);
        }


        if (Auth::attempt($credentials)) {

            $this->clearLoginAttempts($request);

            return response()->json([
                'success' => true
            ]);
        }

        if (!$lockedOut) {
            $this->incrementLoginAttempts($request);
            if ($lockedOut = $this->hasTooManyLoginAttempts($request)) {

                $minutes = ceil($this->limiter()->availableIn($this->throttleKey($request))/60.0);

                return response()->json(['success' => false, 'msgs' =>
                    'Please try again in ' . $minutes . ($minutes == 1 ? ' minute.' : ' minutes.')], 400);
            }
        }

        if ($this->retriesLeft($request) == 1) {
            return response()->json(['success' => false, 'msgs' => 'Invalid credentials. You can try one more time.'], 400);
        }
        return response()->json(['success' => false, 'msgs' => 'Invalid credentials. You can try ' . $this->retriesLeft($request) . ' more times.'], 400);
    }

    private function retriesLeft(Request $request)
    {
        return $this->limiter()->retriesLeft($this->throttleKey($request), $this->maxAttempts());
    }

    protected function username()
    {
        return 'email';
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('login');
    }

    public function checkAuth()
    {
        $loggedIn = Auth::check();

        return response()->json(['success' => true, 'auth' => $loggedIn], 200);
    }
}