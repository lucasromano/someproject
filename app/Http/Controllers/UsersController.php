<?php

namespace SomeProject\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use SomeProject\Repositories\UserRepository;
use SomeProject\Repositories\UserRepositoryEloquent;
use SomeProject\Services\UserService;

class UsersController extends Controller
{
    /**
     * @var UserService
     */
    private $service;
    /**
     * @var UserRepositoryEloquent
     */
    private $repository;

    public function __construct(UserService $service, UserRepository $repository)
    {
        $this->service = $service;
        $this->repository = $repository;
    }

    public function index()
    {
        return $this->service->listUsers();
    }

    public function store(Request $request)
    {
        return $this->service->create($request->json()->all());
    }

    public function update(Request $request, $id)
    {
        return $this->service->update($request->json()->all(), $id);
    }

    public function destroy($id)
    {
        return $this->service->deactivate($id);
    }

    public function restore($id)
    {
        $this->repository->restore($id);

        return ['success' => true];
    }

    public function changeOwnPassword(Request $request)
    {
        return $this->service->changePassword($request->json()->all(), null);
    }

    public function changePassword(Request $request, $id)
    {
        return $this->service->changePassword($request->json()->all(), $id);
    }
}