<?php

namespace SomeProject\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\SomeProject\Repositories\UserRepository::class, \SomeProject\Repositories\UserRepositoryEloquent::class);
        //:end-bindings:
    }
}
