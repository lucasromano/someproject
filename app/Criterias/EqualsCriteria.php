<?php

namespace SomeProject\Criterias;

use Illuminate\Support\Facades\Input;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class EqualsCriteria implements CriteriaInterface
{
    /**
     * @var
     */
    private $field;

    /**
     * EqualsCriteria constructor.
     * @param $field
     */
    public function __construct($field)
    {
        $this->field = $field;
    }

    /**
     * Apply criteria in query repository
     *
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if (Input::has($this->field) && !is_null(Input::get($this->field))) {
            $model = $model->where($this->field, '=', Input::get($this->field));
        }
        return $model;
    }
}