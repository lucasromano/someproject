<?php

namespace SomeProject\Criterias;


class Criteria
{
    /**
     * @param $field
     * @return LikeCriteria
     */
    public static function like($field) {
        return new LikeCriteria($field);
    }

    /**
     * @param $field
     * @return EqualsCriteria
     */
    public static function equals($field) {
        return new EqualsCriteria($field);
    }

    /**
     * @param $from
     * @param $to
     * @return BetweenCriteria
     */
    public static function between($field, $from, $to) {
        return new BetweenCriteria($field, $from, $to);
    }
}