<?php

namespace SomeProject\Criterias;

use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class BetweenCriteria implements CriteriaInterface
{

    /**
     * @var
     */
    private $fromField;
    /**
     * @var
     */
    private $toField;
    /**
     * @var
     */
    private $field;

    public function __construct($field, $from, $to)
    {
        $this->fromField = $from;
        $this->toField = $to;
        $this->field = $field;
    }

    /**
     * Apply criteria in query repository
     *
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {

        if (Input::get($this->fromField)
                && ! is_null(Input::get($this->fromField))) {
            $model = $model->whereDate($this->field, '>=',
                Carbon::parse(Input::get($this->fromField))->format('Y-m-d'));
        }

        if (Input::get($this->toField)
            && ! is_null(Input::get($this->toField))) {
            $model = $model->whereDate($this->field, '<=',
                Carbon::parse(Input::get($this->toField))->format('Y-m-d'));
        }

        return $model;
    }
}