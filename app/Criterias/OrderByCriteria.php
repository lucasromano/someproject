<?php

namespace SomeProject\Criterias;


use Illuminate\Support\Facades\Input;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class OrderByCriteria implements CriteriaInterface
{

    public function apply($model, RepositoryInterface $repository)
    {
        $map = [
            'ascending' => 'ASC',
            'descending' => 'DESC'
        ];

        if (Input::has('orderBy')) {
            return $model->orderBy(Input::get('orderBy'), $map[Input::get('order', 'ascending')]);
        }
        return $model;
    }
}