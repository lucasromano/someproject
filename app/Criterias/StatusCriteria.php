<?php

namespace SomeProject\Criterias;

use Illuminate\Support\Facades\Input;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class StatusCriteria implements CriteriaInterface {

    public function apply($model, RepositoryInterface $repository)
    {
        if (empty(Input::get('status', ''))) {
            return $model->withTrashed();
        }
        if (Input::get('status', '') == 'A') {
            return $model;
        }
        if (Input::get('status', '') == 'I') {
            return $model->onlyTrashed();
        }
    }
}