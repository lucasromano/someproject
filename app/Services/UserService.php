<?php

namespace SomeProject\Services;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Prettus\Validator\Exceptions\ValidatorException;
use SomeProject\Criterias\Criteria;
use SomeProject\Criterias\OrderByCriteria;
use SomeProject\Criterias\StatusCriteria;
use SomeProject\Repositories\UserRepository;
use SomeProject\Repositories\UserRepositoryEloquent;
use SomeProject\Validators\UserValidator;

class UserService
{
    /**
     * @var UserRepositoryEloquent
     */
    private $repository;
    /**
     * @var UserValidator
     */
    private $validator;

    public function __construct(UserRepository $repository, UserValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    public function listUsers()
    {
        $like = explode("|", "name|email");
        $equals = explode("|", "id");

        foreach ($like as $field) {
            $this->repository->pushCriteria(Criteria::like($field));
        }

        foreach ($equals as $field) {
            $this->repository->pushCriteria(Criteria::equals($field));
        }

        $this->repository->pushCriteria(new StatusCriteria());
        $this->repository->pushCriteria(new OrderByCriteria());

        return $this->repository->paginate();
    }

    public function create($inputs)
    {
        try {
            $this->validator->with($inputs)->passesOrFail(UserValidator::RULE_CREATE);

            $inputs['password'] = bcrypt($inputs['password']);
            $this->repository->create($inputs);

            return ['success' => true];
        } catch (ValidatorException $e) {
            return response()->json(['success' => false, 'msgs' => $e->getMessageBag()->all()], 400);
        }
    }

    public function update($inputs, $id)
    {
        $user = $this->repository->skipPresenter()->scopeQuery(function($q) {
            return $q->withTrashed();
        })->find($id);
        if (!Auth::user()->admin && $user->admin) {
            return response()->json(['success' => false, 'msgs' => "You can't edit this user."], 400);
        }

        try {
            unset($inputs['password']);
            unset($inputs['password_confirmation']);

            $this->validator->with($inputs)->setId($id)->passesOrFail(UserValidator::RULE_UPDATE);

            $this->repository->update($inputs, $id);

            return ['success' => true];
        } catch (ValidatorException $e) {
            return response()->json(['success' => false, 'msgs' => $e->getMessageBag()->all()], 400);
        }
    }

    public function deactivate($id)
    {
        if ($id == Auth::id()) {
            return response()->json(['success' => false, 'msgs' => "You can't deactivate your own user."], 400);
        }
        $user = $this->repository->skipPresenter()->scopeQuery(function($q) {
            return $q->withTrashed();
        })->find($id);
        if ($user->admin) {
            return response()->json(['success' => false, 'msgs' => "This user can't be deactivated."], 400);
        }

        $this->repository->delete($id);

        return ['success' => true];
    }

    public function changePassword($inputs, $id = null)
    {
        $user = Auth::user();
        $validation_type = UserValidator::CHANGE_OWN_PASSWORD;

        if (!is_null($id)) {
            $user = $this->repository->skipPresenter()->scopeQuery(function($q) {
                return $q->withTrashed();
            })->find($id);

            if (!Auth::user()->admin && $user->admin) {
                return response()->json(['success' => false, 'msgs' => "You can't change this user's password."], 400);
            }

            $validation_type = UserValidator::CHANGE_PASSWORD;
        }

        try {
            $this->validator->with($inputs)->passesOrFail($validation_type);

            if ($validation_type == UserValidator::CHANGE_OWN_PASSWORD) {
                if (!Hash::check($inputs['password'], $user->password)) {
                    return response()->json(['success' => false, 'msgs' => 'The current password is incorrect.'], 400);
                }
            }

            $data = ['password' => bcrypt($inputs['new_password'])];
            $this->repository->update($data, $user->id);

            return response()->json(['success' => true], 200);
        } catch (ValidatorException $e) {
            $extra = [];
            if ($validation_type == UserValidator::CHANGE_OWN_PASSWORD) {
                if (!Hash::check($inputs['password'], $user->password)) {
                    $extra[] = 'The current password is incorrect.';
                }
            }
            return response()->json(['success' => false, 'msgs' => array_merge($e->getMessageBag()->all(), $extra)], 400);
        }
    }
}