<?php

namespace SomeProject\Validators;

use Prettus\Validator\LaravelValidator;

class UserValidator extends LaravelValidator {

    const CHANGE_OWN_PASSWORD = 'APS';
    const CHANGE_PASSWORD = 'AS';

    protected $rules = [
        UserValidator::RULE_CREATE => [
            'name' => 'required|max:100',
            'email' => 'required|email|max:50|unique:users',
            'password' => 'required|min:8|max:20|confirmed',
            'password_confirmation' => 'required|min:8|max:20'
        ],
        UserValidator::RULE_UPDATE => [
            'name' => 'required|max:100',
            'email' => 'required|email|max:50|unique:users'
        ],
        UserValidator::CHANGE_OWN_PASSWORD => [
            'password' => 'required',
            'new_password' => 'required|min:8|max:20|confirmed',
            'new_password_confirmation' => 'required|min:8|max:20'
        ],
        UserValidator::CHANGE_PASSWORD => [
            'new_password' => 'required|min:8|max:20|confirmed',
            'new_password_confirmation' => 'required|min:8|max:20'
        ]
    ];

}