<?php

namespace SomeProject\Validators;

use Prettus\Validator\LaravelValidator;

class LoginValidator extends LaravelValidator {

    protected $rules = [
        'email' => 'required|email',
        'password' => 'required',
    ];

}