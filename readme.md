# Installation Instructions


## 1) Back-end (Laravel) instructions

##### These instructions are considering that you use Laravel Homestead for Laravel development.

1.1) Add the following lines to your Homestead.yaml ("sites" section):

    - map: someproject.test
      to: /home/vagrant/code/someproject/public
      
1.2) Add the following line to your Homestead.yaml ("databases" section):

    - someproject
    
1.3) Run the Vagrant "provision" command to create the new site and database in your Homestead VM;

1.4) Add the following line to your "hosts" file:

    192.168.10.10 someproject.test

1.5) Inside your Homestead VM (after SSH), navigate to the new mapped project, copy the ".env.example" to a new ".env" file and run the following commands:

    composer install
    php artisan key:generate
    php artisan migrate
    
    
## 2) Front-end (Vue) instructions

2.1) Now, in your local machine, navigate to the project through command line and run:

    npm install
    
2.2) Run the following command to start the application:

    npm run watch
    
2.3) In your browser, navigate to http://localhost:3000. The admin's credentials are:

    E-mail: lucasromanojf@gmail.com
    Password: 12345678