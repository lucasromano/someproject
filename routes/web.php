<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', ['uses' => 'LoginController@index', 'as' => 'login', 'middleware' => 'guest']);
Route::get('/check_auth', ['uses' => 'LoginController@checkAuth']);
Route::post('/login', ['uses' => 'LoginController@login', 'middleware' => 'guest']);

Route::group(['middleware' => 'auth'], function() {
    Route::get('/logout', ['uses' => 'LoginController@logout']);

    Route::put('/change_own_password', ['uses' => 'UsersController@changeOwnPassword']);

    Route::get('/', ['uses' => 'HomeController@index']);

    Route::group(['prefix' => 'users'], function() {
        Route::get('/', 'UsersController@index');
        Route::post('/', 'UsersController@store');
        Route::put('/{id}/change_password', 'UsersController@changePassword');
        Route::put('/{id}', 'UsersController@update');
        Route::delete('/{id}', 'UsersController@destroy');
        Route::put('/{id}/restore', 'UsersController@restore');
    });
});