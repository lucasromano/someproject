
window._ = require('lodash');
window.Popper = require('popper.js').default;

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.$ = window.jQuery = require('jquery');

    require('jquery-numeric');
    require('bootstrap');
} catch (e) {}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo'

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     encrypted: true
// });

//Automating error messages from backend
import { Message, MessageBox } from 'element-ui';
window.axios.interceptors.response.use(function (response) {
    return response;
}, function (error) {
    if (!error.response) {
        Message.error({ message: 'Could not contact server. Please contact the system administrator.', duration: 5000 });
        return Promise.reject(error)
    }
    if (error.response.status === 405 || error.response.status === 500) {
        Message.error({ message: 'Internal system error. Please contact the system administrator.', duration: 5000 });
        return Promise.reject(error)
    }
    if (error.response.status === 400) {
        if (typeof error.response.data.msgs === 'string') {
            Message.warning({ message: error.response.data.msgs, duration: 8000 });
            return Promise.reject(error);
        }
        MessageBox.alert(error.response.data.msgs.join('\n'), { confirmButtonText: 'OK' });
        return Promise.reject(error);
    }
    if (error.response.status === 422) {
        Message.warning({ message: error.response.data.msgs
                ? error.response.data.msgs
                : 'Invalid credentials.', duration: 5000 });
        return Promise.reject(error)
    }
    if (error.response.status === 401 || error.response.status === 419) {
        MessageBox.alert("Session expired.", { confirmButtonText: 'OK' }).then(() => {
            top.location.href = '/login';
    }).catch(() => {
            top.location.href = '/login';
    });
        return Promise.reject(error)
    }
    if (error.response.status === 403) {
        Message.warning({ message: error.response.data.msgs
                ? error.response.data.msgs
                : "You don't have permission to access this resource.", duration: 5000 });
        return Promise.reject(error)
    }
    if (error.response.status === 404) {
        Message.error({ message: 'Resource not found. Please contact system administrator.', duration: 5000 });
        return Promise.reject(error)
    }
});