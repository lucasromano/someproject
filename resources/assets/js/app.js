
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */
import Vue from 'vue';
import App from './App.vue';

import ElementUI from 'element-ui';
import '../sass/element.scss';
import locale from 'element-ui/lib/locale/lang/en';
Vue.use(ElementUI, { locale });

import VueRouter from 'vue-router';
Vue.use(VueRouter);

//Global components
import StatusCombo from './components/StatusCombo.vue';
Vue.component('status-combo', StatusCombo);

// Directives
require('./directives/integer');

import router from './router';

const app = new Vue({
    el: '#app',
    router,
    render: h => h(App)
});