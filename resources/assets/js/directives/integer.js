import Vue from 'vue';
import jQuery from 'jquery';

Vue.directive('integer', {
    bind(el) {
        jQuery(el).find('input').numeric({ decimal: false, negative: false, decimalPlaces: -1 });
    }
});