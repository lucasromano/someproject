import VueRouter from 'vue-router';
import { MessageBox } from 'element-ui';
import { Loading } from 'element-ui';

import Home from './pages/Home.vue';
import UsersModule from './pages/users/Index.vue';

const routes = [
    {
        path: '/',
        component: Home,
        name: 'home'
    },
    {
        path: '/users',
        component: UsersModule,
        name: 'users'
    }
];

const router = new VueRouter({
    routes
});

router.beforeEach((to, from, next) => {

    let loading = Loading.service({ fullscreen: true });

    window.axios({url: '/check_auth'})
        .then(response => {
            loading.close();
            if (response.data.auth) {
                return next();
            } else {
                MessageBox.alert("Session expired.", { confirmButtonText: 'OK' }).then(() => {
                    top.location.href = '/login';
                }).catch(() => {
                    top.location.href = '/login';
                });
                return next(false);
            }
        })
        .catch(() => {
            loading.close()
        });
});

export default router;