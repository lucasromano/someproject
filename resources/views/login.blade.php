<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Some Project</title>
</head>
<body>
<div id="app"></div>

<script src="{{ mix('js/login.js') }}"></script>
</body>
</html>